My Exadel Task 8 project (K8s, Clouds, CI/CD. Just do it!)
======================================================

Important points:
----------------

1. After the completion of development you will show a presentation of the project (Share screen + documentation). *Answer: Presentation will be at 17:30 GMT+03 Minsk Time via Zoom (Share screen + documentation)* 

Tasks:
-----

1. Select an application from the list  (pay attention to the date of the last change in the project ): https://github.com/unicodeveloper/awesome-opensource-apps *Answer: We have selected [MicroWeber](https://github.com/microweber/microweber)*<details><summary>How we analyzed</summary>![img](./images/1.png)![img](./images/2.png)</details>  

2. Select an CI/CD. You can choose any option, but we recommend looking here: 
https://pages.awscloud.com/awsmp-wsm-dev-workshop-series-module3-evolving-to-continuous-deployment-ty.html *Answer: We have chosen Gitlab, because in complex there are CI/CD tools, Docker registry, Runner and integration with GCP.*

3. Select Cloud Service provider for your infrastructure. *We have selected Google Cloud Platform, We got 300$ credit for 90 days*

**What would we like to see?** The created infrastructure in which it will be possible to build, deploy and test the application. *Answer: I developed infrastructure in that it is possible to build, deploy and test the software.*  

**The main things to look out for**

    - Git integration; *Answer: Done (Gitlab)*
    - Setup/configure CI/CD; *Answer: Done (Gitlab)*
    - Application/s should be containerized; *Answer: Done (Docker)*
    - Scheduled backups for DB and all critical data; *Answer: *
    - Logging and monitoring for your services; *Answer: Done (Prometheus, Node Exporter, Grafana, Loki, Promtail)*
    - Security; *Answer: *
    - Use Kubernetes as an orchestration (cloud provider is recommended); *Answer: Done GCP used for k8s*
    - The project must be documented, step-by-step guides to deploy from scratch; *Answer: Done*
    - **EXTRA:** SonarQube integration. *Answer: *


Documentation:
-------------

Microweber – Drag and Drop CMS Laravel Open Source Project.


GitLab CI, CD – part of GitLab that we have used for all of the continuous methods (Continuous Integration, Delivery and Deployment). We were divided into 2 - stages. 
1. Build – build image and push image to GitLab registry (registry.gitlab.com)

2. Deploy – deploy image to running Cluster. You can find more in the given link: [gitlab-ci file](./.gitlab-ci.yml). 


Google CLoud Platform used as Cloud Service Provider for our infrastructure
1. Cluster – we have 2 running Nodes in the cluster. 1 node for microservices, monitoring.

2. Node Affinity used for microservices for assigning them on the exact one node
`kubectl label nodes <node_name> apps=true`


K8S Cluster Configuration (HELM – helps us to manage Kubernetes Application)
 - Ingress Nginx Controller used, working without requiring any extra configuration.
 - By apply below command you will be provided **Nginx Ingress Controller**
```
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```
  -	Additional configurations are needed to make k8s clusters available to use. Cert Manager - you can find more in my written article: [Cert Manager Configurations](https://github.com/Mukhammadamin2002/KubernetesCertManager)

Monitoring and Logging (Helm)
 - Prometheus, Node Exporter, Grafana, Loki, Promtail
 - Visit Following Link To Setup Monitoring on K8S
    - [Setup Kubernetes Monitoring using HELM](https://github.com/Mukhammadamin2002/PrometheusCommunity)


