FROM mitlabs/apache-php7.4

COPY . /app

WORKDIR /app

COPY vhost.conf /etc/apache2/sites-available/000-default.conf

RUN composer install --no-interaction --no-dev --prefer-dist

RUN chmod -R 777 /app

RUN a2enmod rewrite

RUN service apache2 restart
