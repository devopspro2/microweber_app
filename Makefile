#ENV_TAG=latest

build-image:
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull --build-arg BUILDKIT_INLINE_CACHE=1 \
	--cache-from ${REGISTRY}/${PROJECT_NAME}/${APP}:cache \
	--tag ${REGISTRY}/${PROJECT_NAME}/${APP}:cache \
	--tag ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG} .
#	@docker tag ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG} ${REGISTRY}/${PROJECT_NAME}/${APP}:${ENV_TAG}

push-image-cache:
	@docker push ${REGISTRY}/${PROJECT_NAME}/${APP}:cache

push-image:
	@docker push ${REGISTRY}/${PROJECT_NAME}/${APP}:${TAG}
#	@docker push ${REGISTRY}/${PROJECT_NAME}/${APP}:${ENV_TAG}
